Feature: Test Web Functionality

    Scenario: Check Server
        Given Main route
        Then  Return code 200

    Scenario: Check a ping call
        Given Ping http request to /repository/conan-internal/v1/ping
        Then Return code 200

# marvin/ska-example-conan-package/0.0.1-stable

    Scenario: Do a search of the conan-internal repository
        Given an http request to /repository/conan-internal/v1/conans/search
        And a query of ska-example-conan-package
        Then Return code 200
        And one result
        And the result is ska-example-conan-package/0.0.1@marvin/stable

